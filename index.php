﻿<!DOCTYPE html>

<html lang="en">
<head>  
	<meta charset="utf-8">
	<title>Annual Alcoholic Yankee Swap</title>
	
	
<style>

html {
	background: #FBFBFB url(img/bg-light.jpg) left top no-repeat;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
    background-size: cover;

}
body {
	color: #000;
	font: 100%/1.5 Verdana, sans-serif;
	margin: 0 auto;
	width: 50%;
}
	#container {
		margin-left: -25%;
		max-width: 820px;
	}
	
	h1,
	h2 {
		text-shadow: 2px 2px 2px rgba(0,0,0,.3); 
		width: 100%;
	}
	
	h1 {
		color: #ff0000;
		font-size: 30px;
	}
	
	h2 {
		color: #009900;
		font-size: 23px;
	}
	

</style>	

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-99698-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script');
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    ga.setAttribute('async', 'true');
    document.documentElement.firstChild.appendChild(ga);
  })();

</script>



	
	
</head>

<body>

<div id="container">
	
	<h1>Annual Alcoholic Yankee Swap Rules</h1>
	
	
	<p>
	If you'll be joining us for the Annual Alcoholic Yankee Swap, here are the rules! 
	</p>
	
	<h2>Rules for Yankee Swap #1 - The Alcohol</h2>
	<p>Please bring two items with you. The first item should be some sort of alcoholic beverage, costing around $20-$25 (?), and something that can be tasted/sampled/consumed immediately. Use your judgment on the cost, just make it something good.</p>

	<p>Something delicious. Something that will help us get through the holiday season with family and in-laws and also the coming year. You know the deal - make it good. Liquor, Beer, Wine are all acceptable.</p>
	
	
	<h2>Rules for Yankee Swap #2 - The "Miscellaneous" Gift</h2>
	<p>The second item should be a gift of $15 (give or take $5—again, just use your judgement). Potentially the coolest little thing you can possibly find for that price. Something everyone is going to fight over. Something that could make a guy leave his wife just to have it…again, you know the deal.</p>
	
	
	<h2>Misc</h2>
	<p>Both gifts should be completely wrapped with no tags to let people know WHO they are from - this should be anonymous (at least until after everything is opened). You can participate as an individual or as a couple, should you be coupled up. </p>
	
	
	<p>See you there!</p>

	
</div> <!-- /div#container -->

</body>
</html>
